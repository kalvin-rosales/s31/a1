const express = require('express');
const router = express.Router();

const {
	createTask,
	getAllTasks,
	deleteTask,
	updateTask,
	getTask,
	completeTask,
	getSpecificTask,
	findOneTask,
	completeTask

} = require('./../controllers/taskControllers')

//CREATE A TASK
router.post('/', async (req, res) => {
	// console.log(req.body)	{"name": "watching"}	//object

	try{
		await createTask(req.body).then(result => res.send(result))

	} catch(err){
		res.status(400).json(err.message)

	} 
})
 


//GET ALL TASKS
router.get('/', async (req, res) => {
	try{
		await getAllTasks().then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})



//DELETE A TASK

router.delete('/:id/delete', async (req, res) => {
	// console.log(typeof req.params)	//object
	//console.log(typeof req.params.id)	//string

	try{
		await deleteTask(req.params.id).then(response => res.send(response))

	}catch(err){
		res.status(500).json(err)
	}
})



//UPDATE A TASK
router.put('/', async (req, res) => {
	// console.log(req.params.taskId)
	const id = req.params.taskId
	// console.log(req.body)

	// await updateTask(id, req.body).then(response => res.send(response))

	await updateTask(req.body.name, req.body).then(response => res.send(response))
})




// router.get('/:id', async (req, res) => {

// 	const id = req.params.id
// 	try{
// 		await getTask(id, req.body).then(response => res.send(response))
// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })



// router.put('/:id/complete', async (req, res) => {
	
// 	const id = req.params.taskId
	

// 	await completeTask(req.params.id, req.body).then(response => res.send(response))
// })


//find by ID
router.get('/:id', async (req,res) =>{
	//console.log(req.params) //object

	

	try{
		await getSpecificTask(req.params.id).then(result => res.send (result))
	}catch(err){
		res.status(500).json(err)
	}

})

// // find one 
// router.get('/findOne', (req,res) =>{
// 	//console.log(req.body) // {name: ``} // object
// 	findOneTask(req.body.name).then(result => res.send (result))
// })



//Update a specific task to complete

router.put('/:id/complete', async (req,res) =>{

	try{
		await completeTask(req.params.id).then(result => res.send(result))
	} catch (){
		res.status(500).json(err)
	}
})


module.exports = router;


